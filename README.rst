*************
shocking-root
*************

`shocking`_ Plugin for Adding Relative Root Directory to Metadata

.. _`shocking`: https://gitlab.com/shocking/shocking

Configuration
=============

This plugin has no configuration options.

Licensing
=========

The shocking package is licensed under the terms of the `MIT/Expat license`_.

.. _`MIT/expat license`: https://spdx.org/licenses/MIT.html
