__version__ = "0.1.0"
__version_info__ = (0, 1, 0)

import logging

import chevron
from shocking.plugin import BasePlugin

LOGGER = logging.getLogger("shocking")


class ShockingPlugin(BasePlugin):
    """ Calculate a file's path relative to the source directory """

    _defaults = {}

    def prepare(self):
        pass

    def process(self, file_object, **metadata):
        relative_root_path = file_object.path.relative_to(self.config["source"]).parent
        file_object.metadata["root_path"] = relative_root_path
        LOGGER.debug(f"File '{file_object.path}' has relative root of '{relative_root_path}'")

        return file_object
